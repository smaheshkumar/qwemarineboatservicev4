# qwemarineboatservice
QWE Marine boat service

QWE is a small marine service company that carries out the boat maintenance. The company records the total
number of hours for an engine, type of the marine equipment, man-hours of the engineers and related
maintenance.
The aim is to implement a secure on-line system. The prototype contains the following functionality:
 Create service records for an equipment
 Log operational checks
 Log of software upgrades for a navigation system
 List of Repairs for a vessel
 Log of safety inspections for a vessel

Please click on below links to access the various pages of the website prototype created for QWE Marine Boat Service

Home Page : https://github.coventry.ac.uk/pages/sunnappum/qwemarineboatservicev3/index.html

Vesel Repairs: https://github.coventry.ac.uk/pages/sunnappum/qwemarineboatservicev3/Repairs.html

Safety Inspections of Vessel: https://github.coventry.ac.uk/pages/sunnappum/qwemarineboatservicev3/Safety.html

Operational Checks : https://github.coventry.ac.uk/pages/sunnappum/qwemarineboatservicev3/operationalchecks.html

Equipment service records: https://github.coventry.ac.uk/pages/sunnappum/qwemarineboatservicev3/servicerecords.html

Software Upgrade for Navigation System : https://github.coventry.ac.uk/pages/sunnappum/qwemarineboatservicev3/softwareupgrade.html

As this is a prototype model of the application there are known issues which are captured as part of testing and issues are raised respectively in GITHUB (For developers) and JIRA( for Business team).

Issue1. The signup form is not correctly aligned for view
Issue2: X close button for the signup form is not closing the state even after clicking it.
Issue 3: Clicking on the Menu Options it’s not redirecting to the respective page
Issue 4: The login form doesn’t display error messages per wireframe.
Issue 5: No proper alignment of the Login and Signup buttons as per wireframes
